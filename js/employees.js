app.factory('Employees', function() {
    var Employees = {};
    Employees.list = [
        {
            name: "John",
            last: "Kowalski",
            born: new Date(1982, 04, 01),
            hobby: "Muzyka, Prawo",
            sex: "Mężczyzna",
            img: "img/1.jpg"
        },
        {
            name: "Piotr",
            last: "Nowak",
            born: new Date(1973, 09, 12),
            hobby: "Film, Muzyka",
            sex: "Mężczyzna",
            img: "img/2.jpg"
        },
        {
            name: "Paweł",
            last: "Kuźniar",
            born: new Date(1982, 01, 15),
            hobby: "Taniec, Film",
            sex: "Mężczyzna",
            img: "img/3.jpg"
        },
        {
            name: "Michał",
            last: "Rybarczyk",
            born: new Date(1962, 02, 21),
            hobby: "Malarstwo, Podróże",
            sex: "Mężczyzna",
            img: "img/4.jpg"
        },
        {
            name: "Karolina",
            last: "Kowalska",
            born: new Date(1984, 05, 31),
            hobby: "Muzyka, Podróże",
            sex: "Kobieta",
            img: "img/5.jpg"
        },
        {
            name: "Anna",
            last: "Iksińska",
            born: new Date(1991, 12, 20),
            hobby: "Programowanie, Fotografia",
            sex: "Kobieta",
            img: "img/6.jpg"
        },
        {
            name: "Michalina",
            last: "Szpak",
            born: new Date(1988, 11, 22),
            hobby: "Muzyka, Fotografia",
            sex: "Kobieta",
            img: "img/7.jpg"
        },
        {
            name: "Mateusz",
            last: "Wróblewski",
            born: new Date(1985, 08, 25),
            hobby: "Film, Sport",
            sex: "Mężczyzna",
            img: "img/8.jpg"
        },
        {
            name: "Karol",
            last: "Mech",
            born: new Date(1978, 07, 18),
            hobby: "Muzyka, Sport",
            sex: "Mężczyzna",
            img: "img/9.jpg"
        },
        {
            name: "Alicja",
            last: "Lipiec",
            born: new Date(1968, 03, 14),
            hobby: "Muzyka, Film",
            sex: "Kobieta",
            img: "img/10.jpg"
        },
        {
            name: "Grażyna",
            last: "Patyk",
            born: new Date(1974, 04, 02),
            hobby: "Programowanie, Sport",
            sex: "Kobieta",
            img: "img/11.jpg"
        },
        {
            name: "Łukasz",
            last: "Grzegorczyk",
            born: new Date(1982, 08, 05),
            hobby: "Gry, Muzyka",
            sex: "Mężczyzna",
            img: "img/12.jpg"
        },
        {
            name: "Tomasz",
            last: "Januszewski",
            born: new Date(1979, 05, 09),
            hobby: "Grafika, Programowanie",
            sex: "Mężczyzna",
            img: "img/13.jpg"
        },
        {
            name: "Maciej",
            last: "Bąk",
            born: new Date(1976, 12, 13),
            hobby: "Film, Gry",
            sex: "Mężczyzna",
            img: "img/14.jpg"
        },
        {
            name: "Wojciech",
            last: "Rogal",
            born: new Date(1980, 02, 18),
            hobby: "Sport, Prawo",
            sex: "Mężczyzna",
            img: "img/15.jpg"
        },
    ];
    return Employees;
});

app.controller('employeesCtrl', function($scope, Employees) {
    $scope.employees = Employees;
});

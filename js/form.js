app.controller('ContactForm', function ($scope) {
    var defaultForm = {
        name: "",
        email: "",
        text: ""
    };
    $scope.reset = function (message) {
        $scope.contactForm.$setPristine();
        $scope.message = defaultForm;
    };
    $scope.send = function (message) {
        $scope.outmessage = $scope.message;
    }
});

var app = angular.module('myApp', ['ngRoute']);

app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'partials/welcome.html',
            controller: 'greetings'
        }, null)
        .when('/lista', {
            templateUrl: 'partials/employees.html',
            controller: 'employeesCtrl'
        }, null)
        .when('/kontakt', {
            templateUrl: 'partials/form.html',
            controller: 'employeesCtrl'
        }, null)
        .otherwise({
            redirectTo: '/'
        });
}]);


app.controller('greetings', function ($scope) {
    $scope.title = "Witaj na stronie projektu";
    $scope.desc = "Została ona wykonana przez Mateusza Kuźnik";

});

app.filter('age', function () {
    function calculateAge(birthday) { // birthday is a date
        var ageDifMs = Date.now() - birthday.getTime();
        var ageDate = new Date(ageDifMs); // miliseconds from epoch
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    }

    return function (birthdate) {
        return calculateAge(birthdate);
    };
});

app.directive('birthday', function () {
    var today = (new Date()).getFullYear();
    return {
        restrict: 'A',
        replace: true,
        scope: {
            date: '='
        },
        template: '<td> {{ date.born | date:"dd.MM.yyyy"}} <span class="text-primary">({{date.born | age}})</span></td>'
    };
});
